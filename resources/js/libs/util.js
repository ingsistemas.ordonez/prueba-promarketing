import _ from 'lodash';

export function formatMoney (valueToFormat) {
    return new Intl.NumberFormat("es-CO").format(valueToFormat);
}

export function formatNumber (valueToFormat) {
    return new Intl.NumberFormat("de-DE").format(valueToFormat);
}