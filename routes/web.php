<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\OrdenPagoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Autenticacion de usuario
Route::post('/login', 'Auth\LoginController@login');
Route::post('/logout', 'Auth\LoginController@logout');
Route::post('/resetPassword', 'Auth\ResetPasswordController@sendMail');

Route::group(['middleware' => ['auth']], function() {   

    Route::get('/logout', 'Auth\LoginController@logout');
    
    Route::get('/dashboard', function () {
        return view('app'); 
    });

    Route::get('/user-login', function () {
        return Auth::user();
    });    

    Route::get('/home/index', [HomeController::class, 'valores'])->name('valores');
    
    Route::get('/ordenes/index', [OrdenPagoController::class, 'index']);
    Route::get('/ordenes/{id}', [OrdenPagoController::class, 'get']);
    Route::put('/ordenes/update/{id}', [OrdenPagoController::class, 'update']);  
    Route::delete('/ordenes/{id}', [OrdenPagoController::class, 'destroy']);
    
    Route::get('/users/index', [UserController::class, 'index']);
    Route::post('/users', [UserController::class, 'create']);  
    Route::get('/users/{id}', [UserController::class, 'get']);
    Route::put('/users/update/{id}', [UserController::class, 'update']);  
    Route::delete('/users/{id}', [UserController::class, 'destroy']);
});

Route::get('/{optional?}', function () {
    if (Auth::check()) {
        return view('app');
    } else {
        return view('app');
    }
})->name('basepath');

Auth::routes();
