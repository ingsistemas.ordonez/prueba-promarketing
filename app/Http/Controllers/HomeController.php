<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function valores(Request $request) 
    {
        if(!$request->ajax()) return redirect('/');         

        return response()->json([
            'tipos' => 0,
            'procesos' => 0,
            'documentos' => 0
        ], 200);  

    }
}
