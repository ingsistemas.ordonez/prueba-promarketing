<style>

    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }

      td, th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
      }

      tr:nth-child(even) {
        background-color: #dddddd;
      }
        </style>
    <div class="row">
        <div class="col-12 text-center">
            <h2 class="sub-titulo pt-3 pb-1">Restauración de contraseña</h2>
            <h3>Se te a asignado una contraseña aleatoria</h3>
            <p><b>{{ $password }}</b></p>
            <p>Ingresa y asigna tu nueva contraseña aquí:</p>
        </div>
        <br>
        <div class="col-12">
            <table>
                <tr>
                    <td>
                        {{$url}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
