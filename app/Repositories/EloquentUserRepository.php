<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Log;

/**
 * Class EloquentUserRepository.
 *
 * @package namespace App\Repositories;
 */
class EloquentUserRepository extends BaseRepository implements UserRepository
{
    public function model()
    {
        return \App\Models\User::class;
    }

    public function all($columns = ['*'])
    {
        return parent::all($columns);
    }

    public function findBy(array $conditions = [])
    {
        return $this->model->where($conditions)->get();
    }

    public function create($data)
    {
        return parent::create($data);
    }

    public function update(array $data, $id)
    {
        return parent::update($data, $id);
    }

    public function delete($id)
    {
        return parent::destroy($id);
    }
}