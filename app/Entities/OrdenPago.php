<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class OrdenPago.
 *
 * @package namespace App\Entities;
 */
class OrdenPago extends Model implements Transformable
{
    use TransformableTrait;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'amount',
        'description',
        'id_seller',
        'status',
    ];

}
