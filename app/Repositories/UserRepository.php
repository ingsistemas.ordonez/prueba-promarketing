<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

interface UserRepository extends RepositoryInterface
{
    public function all($columns = ['*']);
    public function find($id, $columns = ['*']);
    public function update(array $attributes, $id);
    public function delete($id);
    public function create(array $data);
}