/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */
const mix = require('laravel-mix');

mix
.styles([
    'resources/plugins/fontawesome-free/css/all.min.css',
    'resources/css/adminlte.min.css',
    'resources/css/custom.css',
], 'public/css/all.css')
.js('resources/js/app.js','public/js') //Jquery, Bootstrap, Vuejs
.scripts([
    'resources/js/adminlte.min.js',
    'resources/js/demo.js',
], 'public/js/all.js')
.copy('resources/plugins/fontawesome-free/webfonts', 'public/webfonts')
.copy('resources/img', 'public/img');
