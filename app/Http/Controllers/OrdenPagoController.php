<?php

namespace App\Http\Controllers;

use App\Repositories\OrdenPagoRepository;
use Illuminate\Http\Request;

class OrdenPagoController extends Controller
{
    protected $ordenPagoRepository;

    public function __construct(OrdenPagoRepository $ordenPagoRepository)
    {
        $this->ordenPagoRepository = $ordenPagoRepository;
    }

    public function createOrder(Request $request)
    {
        try {
            $data = $request->validate([
                'amount' => 'required|numeric',
                'description' => 'required|string',
                'id_seller' => 'required|numeric',
            ]);

            $ordenPago = $this->ordenPagoRepository->createOrder($data);

            return response()->json([
                'id_orden' => $ordenPago->id,
                'url_redireccion' => 'tu_url_de_redireccion',
                'date_expiration' => $ordenPago->created_at->addDays(7)->toDateTimeString(),
            ], 201);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving order'], 500);
        }
    }

    public function confirmOrder(Request $request)
    {
        try {
            $data = $request->validate([
                'amount' => 'required|numeric',
                'id_order' => 'required|numeric',
                'id_seller' => 'required|numeric',
            ]);

            $status = $this->ordenPagoRepository->confirmOrder($data);

            return response()->json([
                'status' => $status
            ], 201);

        } catch (\Illuminate\Validation\ValidationException $e) {
            $errors = $e->errors();
            return response()->json(['errors' => $errors], 422);
        } catch (\Exception $e) {
            return response()->json(['errors' => 'Error retrieving user'], 500);
        }
    }

    public function index(Request $request)
    {
        try {
            $status = $request->input('status');
            $created_at = $request->input('created_at');
            $id = $request->input('id');
            
            $conditions = [];
            if ($status) {
                $conditions[] = ['status', '=', $status];
            }
            if ($created_at) {
                $conditions[] = ['created_at', 'LIKE', $created_at . '%'];
            }
            if ($id) {
                $conditions[] = ['id', '=', $id];
            }

            $data = $this->ordenPagoRepository->findBy($conditions);

            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving order'], 500);
        }
    }

    public function get($id)
    {
        try {
            $orden = $this->ordenPagoRepository->find($id, ['amount', 'description', 'id_seller', 'created_at', 'status']);

            if ($orden) {
                return response()->json($orden, 200);
            } else {
                return response()->json(['error' => 'Order not found'], 404);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving order'], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            // Validar los datos del request
            $request->validate([
                'amount' => 'required|numeric',
                'description' => 'required|string',
                'status' => 'required|string',
                'id_seller' => 'required|numeric',
            ]);

            // Obtener los datos actualizados del request
            $data = [
                'amount' => $request->input('amount'),
                'description' => $request->input('description'),
                'id_seller' => $request->input('id_seller'),
                'status' => $request->input('status'),
            ];

            // Actualizar la orden de pago utilizando el repositorio
            $updatedOrder = $this->ordenPagoRepository->update($data, $id);

            return response()->json([
                'message' => 'Orden de pago actualizada con éxito',
                'order' => $updatedOrder,
            ], 200);

        } catch (\Illuminate\Validation\ValidationException $e) {
            $errors = $e->errors();
            return response()->json(['errors' => $errors], 422);
        } catch (\Exception $e) {
            return response()->json(['errors' => 'Error retrieving user'], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $this->ordenPagoRepository->delete($id);
            return response()->json(['message' => 'Orden eliminada correctamente'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving order'], 500);
        }
    }
}
