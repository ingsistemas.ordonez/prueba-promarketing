<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\Repositories\OrdenPagoRepository;
/**
 * Class OrdenPagoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class OrdenPagoRepositoryEloquent extends BaseRepository implements OrdenPagoRepository
{
    public function model()
    {
        return \App\Entities\OrdenPago::class;
    }
    public function createOrder($data)
    {
        return $this->create($data);
    }
    public function confirmOrder($data)
    {
        $order = $this->model
            ->where('id', $data['id_order'])
            ->where('id_seller', $data['id_seller'])
            ->where('amount', $data['amount'])
            ->first();

        if (!$order) {
            return 'NOT_FOUND';
        }

        return $order->status;
    }

    public function all($columns = ['*'])
    {
        return parent::all($columns);
    }

    public function findBy(array $conditions = [])
    {
        return $this->model->where($conditions)->get();
    }

    public function update(array $data, $id)
    {
        return parent::update($data, $id);
    }

    public function delete($id)
    {
        return parent::destroy($id);
    }
}

