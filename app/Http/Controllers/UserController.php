<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function index(Request $request)
    {
        try {
            $conditions = $this->buildConditions($request);

            $data = $this->userRepository->findBy($conditions, ['id', 'name', 'email', 'created_at', 'estado']);

            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving user'], 500);
        }
    }

    private function buildConditions(Request $request)
    {
        $status = $request->input('status');
        $created_at = $request->input('created_at');
        $id = $request->input('id');

        $conditions = [];

        if ($status) {
            $conditions[] = ['status', '=', $status];
        }
        if ($created_at) {
            $conditions[] = ['created_at', 'LIKE', $created_at . '%'];
        }
        if ($id) {
            $conditions[] = ['id', '=', $id];
        }

        return $conditions;
    }

    public function create(Request $request)
    {
        try {
            $data = $this->validateUserData($request->all());

            $data['password'] = Hash::make($data['password']);

            $this->userRepository->create($data);

            return response()->json([
                'message' => 'Usuario creado con éxito',
            ], 200);
        } catch (\Illuminate\Validation\ValidationException $e) {
            $errors = $e->errors();
            return response()->json(['errors' => $errors], 422);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Error retrieving user'], 500);
        }
    }

    private function validateUserData(array $data)
    {
        return validator($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|unique:users|max:255',
            'password' => 'required|string|min:6',
            'estado' => 'required|string|max:255',
        ])->validate();
    }

    public function get($id)
    {
        try {
            $res = $this->userRepository->find($id, ['name', 'email', 'estado', 'created_at']);

            if ($res) {
                return response()->json($res, 200);
            } else {
                return response()->json(['error' => 'User not found'], 404);
            }

        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving user'], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data = $this->validateUpdateData($request->all());

            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            } else {
                unset($data['password']);
            }

            $res = $this->userRepository->update($data, $id);

            return response()->json([
                'message' => 'Usuario actualizado con éxito',
                'order' => $res,
            ], 200);

        } catch (\Illuminate\Validation\ValidationException $e) {
            $errors = $e->errors();
            return response()->json(['errors' => $errors], 422);
        } catch (\Exception $e) {
            Log::error($e);
            return response()->json(['error' => 'Error retrieving user'], 500);
        }
    }

    private function validateUpdateData(array $data)
    {
        return validator($data, [
            'name' => 'required|string|max:255',
            'password' => 'nullable|string|min:6',
            'estado' => 'required|string|max:255',
        ])->validate();
    }

    public function destroy($id)
    {
        try {
            $this->userRepository->delete($id);
            return response()->json(['message' => 'Usuario eliminado correctamente'], 200);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error retrieving user'], 500);
        }
    }
}
