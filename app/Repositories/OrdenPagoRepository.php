<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrdenPagoRepositoryRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrdenPagoRepository extends RepositoryInterface
{
    public function createOrder($data);
    public function confirmOrder($id);    

    public function all($columns = ['*']);
    public function find($id, $columns = ['*']);
    public function update(array $attributes, $id);
    public function delete($id);
}