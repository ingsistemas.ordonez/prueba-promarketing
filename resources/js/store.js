import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        newUser: [],   
    },
    getters: {
        getNewUser: state => {
            return state.newUser;
        },
    },
    mutations: {
        CREATE_NEW_USER: (state, data) => {
            console.log('CREATE_NEW_USER', data)
            state.newUser = data;
        }, 
    },
    actions: {
        setNewUser: ({ commit }, data) => {
            commit('CREATE_NEW_USER', data)
        },    
    }
});