<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdenPagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_pagos', function (Blueprint $table) {
            $table->id();
            $table->decimal('amount', 8, 2);
            $table->string('description')->default('')->nullable();
            $table->unsignedBigInteger('id_seller');
            $table->enum('status', ['CREATE', 'PENDING', 'SUCCESS', 'EXPIRED', 'REJECTED'])->default('CREATE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orden_pagos');
    }
}
