# Promarketing  Prueba Técnica
Prueba de Jonathan Ordoñez Bello de prueba técnica práctica de PHP para el puesto de Desarrollador Laravel

## 1. Instalación
Para comenzar a utilizar la prueba, sigue estos pasos:

Clona este repositorio en tu computadora local:

git clone git@gitlab.com:ingsistemas.ordonez/prueba-promarketing.git

## 2. Abre una terminal en la carpeta del repositorio clonado e instala las dependencias de PHP y JavaScript utilizando Composer y npm:

composer install
npm install

## 3. Cambia el nombre del archivo prueba.env a .env y configura las variables de entorno de la aplicación si son diferentes a las de prueba o ejecute el comando cp .env.example .env 


DB_CONNECTION=mysql

DB_HOST=127.0.0.1

DB_PORT=3306

DB_DATABASE=promarketing

DB_USERNAME=

DB_PASSWORD=

## 4. Ejecuta las migraciones para crear las tablas de la base de datos, con el siguiente comando creara tambien los registros base:

php artisan migrate

## 5. Genera los archivos CSS y JavaScript de la aplicación utilizando Laravel Mix:

npm run dev

## 6. Iniciar la aplicación utilizando el servidor web incorporado de Laravel:

php artisan serve

## 7. Accede a la aplicación en tu navegador web en la URL http://localhost:8000 o http://127.0.0.1:8000.

## 8. Para el login son las siguientes credenciales soporte@gmail.com : 123456
## Requerimientos del sistema

Para utilizar la prueba, se requiere tener instalado lo siguiente:

- [PHP 8.2 o superior]
- [Composer]
- [MySQL 5.7 o superior]
- [Node.js 10 o superior]
- [npm 6 o superior]

## Dependencias

Utiliza las siguientes dependencias principales:

- [Laravel 8.x]
- [Vue.js 2.x]
- [Bootstrap 5.x]

# API de Orden de Pagos

Esta API proporciona endpoints para la creación y confirmación de órdenes de pago.

## Rutas Disponibles

### 1. Crear Orden de Pago

- **Método:** POST
- **URL:** /api/create-order
- **Controlador:** OrdenPagoController
- **Método del Controlador:** createOrder

#### Parámetros de Entrada

La solicitud debe incluir los siguientes parámetros en formato JSON:

```json
{
  "amount": 100.00,
  "description": "Descripción de la orden",
  "id_seller": 1
}
```

Respuesta Exitosa

```json
{
  "id_orden": 123,
  "url_redireccion": "tu_url_de_redireccion",
  "date_expiration": "2023-12-31T23:59:59"
}
```

### 2. Confirmar Orden de Pago

- **Método:** POST
- **URL:** /api/confirm-order
- **Controlador:** OrdenPagoController
- **Método del Controlador:** confirmOrder
- **Parámetros de Entrada**
- **La solicitud debe incluir los siguientes parámetros en formato JSON:**

```json
{
  "id_order": 123,
  "id_seller": 1,
  "amount": 100.00
}
```

Respuesta Exitosa

```json
{
  "status": "SUCCESS"
}
```